# Set the location of Microchip XC8 MPLABX for programming
# Ensure that the version is also included if there are
# multiple versions installed.
MPLABX=/opt/microchip/mplabx/v6.00/

# Set the Microchip xc8 compiler location
CC=/opt/microchip/xc8/v2.36/bin/xc8-cc

# Set the target PIC device
DEV=16F18346

# Set the programming device being used. Run target
# ipe-help for a list of valid options (see the T
# switch in the help). Then run target ipe-ls to
# see a list of connected targets.
PROG=PKOB

JAVA=$(shell find $(MPLABX) -type f -name java)
IPENAME=$(shell find $(MPLABX) -type f -name ipecmd.jar)
IPECMD=$(JAVA) -jar $(IPENAME)

# Flags to the XC8 compiler
CFLAGS = -mcpu=$(DEV) -mwarn=3 -mdefault-config-bits -DPIC$(DEV) -O2 -mext=cci -std=c99

# Header include directories (include the -I)
IFLAGS =

# Source search paths (locations containing .c/.h)
VPATH = src

# Messages about CFLAGS and LFLAGS (see patterns.mk)
MESSAGES = opt warn parallel lib debug cflags lflags

# Dependency directories
DEPS = deps
OBJDIR = obj

# Executables list
EXE = output test cmix conly

# List output executables with object file dependencies
main.hex: main.p1 obj/io.p1 obj/tick.p1 obj/interrupts.p1

program: main.hex
	$(IPECMD) -P$(DEV) -TP$(PROG) -M -Y -OL -L -Fmain.hex

ipe-erase:
	$(IPECMD) -P$(DEV) -TP$(PROG) -E

ipe-blankcheck:
	$(IPECMD) -P$(DEV) -TP$(PROG) -C

ipe-help:
	$(IPECMD) -?

ipe-ls:
	$(IPECMD) -T

xc8-help:
	$(CC) --help

test:
	$(CC) -c $(CFLAGS) src/main.c -MMD 


# Include makefile patterns and dependency information
include patterns.mk
-include $(wildcard $(DEPS)/*.d)

clean:
	rm -rf *.d *.p1 *.elf *.sym *.rlf *.lst *.o *.s *.cmf *.hxl *.sdb *.hex
