#ifndef CONFIG_H
#define CONFIG_H

#pragma config WDTE = OFF
#pragma config RSTOSC = HFINT32 // Use the 32MHz internal oscillator
#pragma config FEXTOSC = OFF // Otherwise RA5 won't work

#define _XTAL_FREQ 32000000

#endif
