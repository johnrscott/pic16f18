#include <xc.h>

#include "config.h"

#include "io.h"
#include "tick.h"
#include "interrupts.h"

void main(void)
{
    setup_interrupts();
    setup_board_io();
    setup_tick();

    while(1) {
	set_all_leds(0);
	delay(1);
	set_all_leds(1);
        delay(1);
    }
}
