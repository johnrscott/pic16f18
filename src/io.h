/**
 * @file io.h
 * @brief Basic input output functions (LEDs, buttons)
 *
 * This file corresponds to the LPC Curiosity board. See the
 * documentation schematics for details of the pinout.
 */

#ifndef IO_H
#define	IO_H

    /**
     * @brief Run before using the development board IO functions
     * 
     * Prerequisites: none
     */
    void setup_board_io(void);  

    /**
     * @brief Turn all the LEDs on or off
     * @param state 1 for on, 0 for off. Other values undefined
     */
    void set_all_leds(unsigned char state);

    /**
     * @brief Toggle the state of D4
     * 
     * If D4 is off, turn it on. If it is on, turn it off.
     */
    void toggle_d4(void);
    
    void toggle_d6(void);
    
#endif
