#include <xc.h>

#include "io.h"
#include "tick.h"

/**
 * @brief Global interrupt service routine.
 * 
 * This function is called automatically when any interrupt
 * is raied in the PIC16f, provided that setup_interrupts() has
 * been run.
 * 
 * 
 */
void __interrupt() isr(void) {

    // Check for the timer 0 interrupt for the ticker
#ifdef PIC16F18346
    if (PIR0bits.TMR0IF == 1) {
        tick_increment();
	PIR0bits.TMR0IF = 0; // Clear the interrupt
    }
#elif PIC16F18446
    // TODO
#endif  
    
    return;
}

void setup_interrupts(void)
{
    // Enable peripheral interrupts
    INTCONbits.PEIE = 1;
        
    // Globally enable interrupts
    INTCONbits.GIE = 1;
}
