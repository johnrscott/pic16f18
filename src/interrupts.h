/**
 * @file interrupts.h
 * @brief Set up peripheral interrupts
 * 
 */

#ifndef INTERRUPTS_H
#define	INTERRUPTS_H

/**
 * @brief Setup and enable interrupts globally
 * 
 * Prerequisites: none
 * 
 * This function enables interrupts globally, and enables
 * peripheral interrupts. To enable a peripheral interrupt, 
 * make sure this function has been called, and then set 
 * the interrupt enable bit in the correct PIEx register 
 * for the peripheral.
 * 
 * 
 */
void setup_interrupts(void);


#endif

