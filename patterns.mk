# Copyright 2022 John Scott
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

## CONFIGURATION ===========================================================

# MESSAGE PRINTING
#
# The makefile can automatically parse common options in CFLAGS
# and LFLAGS. To print messages, copy the MESSAGES variable to Makefile
# as follows with a list of what should be printed.
#
#  MESSAGES = opt warn debug
#
# The possible messages are as follows
#
# - opt      	print the optimisation level (e.g. -O2 or -O3)
# - warn     	print warning flags beginning with -W (e.g. -Wall)
# - debug    	print debug flags beginning with -g
# - lib      	print libraries linked using -l in LFLAGS
# - parallel    print -fopenmp flag if present
# - cflags      print CFLAGS that are not printed by other messages
# - lflags      print LFLAGS that are not printed by other messages
#
#

# ABBREVIATED COMPILATION AND LINKING
#
# Use this variable to decide whether compilation and linking steps should
# be printed in full or whether an abbreviated version should be printed instead.
#
# For example, for a full compilation line:
#
#   g++ -c -O2 -Wall src/file.cpp -o output.o, 
#
# the abbreviated version will print
#
#  CC  std/file.cpp -o output.o
#
# Set to YES to use abbreviated printing (or anything else to disable)
#
ABBREV=NO

## COLOUR CODES AND STYLING ================================================

# Foreground and background colours
GREEN_FG = $(shell tput setaf 2)
GREEN_BG = $(shell tput setab 2)
YELLOW_FG = $(shell tput setaf 3)
YELLOW_BG = $(shell tput setab 3)
RED_FG = $(shell tput setaf 1)
RED_BG = $(shell tput setab 1)

# Styling and reset
BOLD = $(shell tput bold)
RESET = $(shell tput sgr0)

## MESSAGE PRINTING ========================================================

AVAILABLE_MESSAGES = warn opt parallel debug lib cflags lflags
RECOGNISED_MESSAGES = $(filter $(AVAILABLE_MESSAGES), $(MESSAGES))

# Make a copy of CFLAGS
OTHER_CFLAGS = $(CFLAGS)
OTHER_LFLAGS = $(LFLAGS)

# Filter CFLAGS
ifneq ($(filter parallel, $(RECOGNISED_MESSAGES)),)
OTHER_CFLAGS := $(filter-out -fopenmp, $(OTHER_CFLAGS))
endif
ifneq ($(filter warn, $(RECOGNISED_MESSAGES)),)
OTHER_CFLAGS := $(filter-out -W%, $(OTHER_CFLAGS))
endif
ifneq ($(filter opt, $(RECOGNISED_MESSAGES)),)
OTHER_CFLAGS := $(filter-out -O%, $(OTHER_CFLAGS))
endif
ifneq ($(filter debug, $(RECOGNISED_MESSAGES)),)
OTHER_CFLAGS := $(filter-out -D% -g% -pg, $(OTHER_CFLAGS))
endif

# Filter LFLAGS
ifneq ($(filter lib, $(RECOGNISED_MESSAGES)),)
OTHER_LFLAGS := $(filter-out -l%, $(OTHER_LFLAGS))
endif

print_parallel:
	@printf '$(BOLD)Parallel:\t$(RESET) '
ifeq ($(parallel_flags),)
	@printf '$(YELLOW_BG)none$(RESET)\n'
else
	@printf '$(GREEN_BG)$(parallel_flags)$(RESET)\n'
endif

# Check library flags
lib_flags = $(filter -l%, $(LFLAGS))
print_lib:
	@printf '$(BOLD)Libraries:\t$(RESET) '
ifeq ($(lib_flags),)
	@printf 'none\n'
else
	@printf '$(lib_flags)\n'
endif

# Check optimisation level
opt_flags = $(filter -O%, $(CFLAGS))
print_opt:
	@printf '$(BOLD)Optimisation:\t$(RESET) '
ifeq ($(opt_flags),-O1)
	@printf '$(YELLOW_BG)-O1$(RESET)\n'
else ifeq ($(opt_flags),-O2)
	@printf '$(GREEN_BG)-O2$(RESET)\n'
else ifeq ($(opt_flags),-O3)
	@printf '$(GREEN_BG)-O3$(RESET)\n'
else
	@printf '$(RED_BG)none$(RESET)\n'
endif

# Check the warnings
warn_flags = $(filter -W%, $(CFLAGS))
print_warn:
	@printf '$(BOLD)Warnings:\t$(RESET) '
ifeq ($(warn_flags),)
	@printf '$(YELLOW_BG)none$(RESET)\n'
else
	@printf '$(GREEN_BG)$(warn_flags)$(RESET)\n'
endif

# Check debug flags
debug_flags = $(filter -D% -g% -pg, $(CFLAGS))
print_debug:
	@printf '$(BOLD)Debugging:\t$(RESET) '
ifeq ($(debug_flags),)
	@printf '$(GREEN_BG)none$(RESET)\n'
else
	@printf '$(YELLOW_BG)$(debug_flags)$(RESET)\n'
endif

# Print the other flags
print_cflags:
	@printf '$(BOLD)Other CFLAGS:\t$(RESET) '
ifeq ($(OTHER_CFLAGS),)
	@printf 'none\n'
else
	@printf '$(OTHER_CFLAGS)\n'
endif

# Print the other flags
print_lflags:
	@printf '$(BOLD)Other LFLAGS:\t$(RESET) '
ifeq ($(OTHER_LFLAGS),)
	@printf 'none\n'
else
	@printf '$(OTHER_LFLAGS)\n'
endif


# Print a newline before messages
.PHONY: MSG_NEWLINE
MSG_NEWLINE:
ifneq ($(MESSAGES),)
	@echo
endif

.PHONY: FILTER_MESSAGES
FILTER_MESSAGES:
ifneq ($(filter-out $(AVAILABLE_MESSAGES), $(MESSAGES)),)
	@printf "\n$(YELLOW_FG)$(BOLD)Makefile warning: $(RESET)"
	@printf "'$(filter-out $(AVAILABLE_MESSAGES), $(MESSAGES))'"
	@printf " unrecognised in MESSAGES variable"
	@echo
endif

.PHONY: PRINT_INFO
PRINT_INFO: FILTER_MESSAGES MSG_NEWLINE $(addprefix print_, $(RECOGNISED_MESSAGES))
ifneq ($(MESSAGES),)
	@echo
endif

## AUTOMATIC DEPENDENCIES =================================================

# These flags are used for object file compilation.
# When g++ sees these flags, it generates a list of
# all the header files required by the object file
# being compiled, and stores them in a file specified
# by the -MF flag. This list is in a Makefile
# compatible format, so these dependencies can just
# be included directly into the main Makefile.
#
# Note: I think there's a problem with having $* here
# if you want to put special case rules. Need to think
# about it.
#
#DFLAGS = -MMD -MT $*.p1 -MF $(DEPS)/$*.d
DFLAGS = -MMD -MF $(DEPS)/$*.d

# Make requisite directories
#
# This target makes the object and dependency directories
# They have the names given in the $(OBJDIR) and $(DEPS)
# variables. They are created if they do not exist, and
# left alone if they do.
#
$(OBJDIR) $(DEPS):
	mkdir -p $@

## DIRECTORY UTILITIES =====================================================

# Add object directory to search path
#
# For this version of the makefile, which uses the
# object file names rather than their full paths, it
# is necessary to specify the object file directory
# as a search path so that make can find object file
# prerequisites that already exist.
#
vpath %.o $(OBJDIR)

# Fix the object file paths
#
# This function is used in the linking step to
# filter the prerequisite list and append the
# correct object file path
#
define fixPath
$(addprefix $(OBJDIR)/, $(notdir $(filter %.p1, $(1))))
endef

## LINKING ================================================================

# Set up abbreviated printing
Q=
ifneq ($(filter YES, $(ABBREV)),)
Q=@
endif

# This pattern is used for the compiling the final 
# executable. To use it, put a line in the main
# Makefile like:
#
#   output: source1.o source2.o
#
# (Do not supply the compilation line.) Then the 
# rule below will match `output', using output.o 
# as an additional dependency, before linking together 
# source1.o,  source2.o and output.o into the output
# executable.
#
%:
ifneq ($(filter @, $(Q)),)
	@echo "LD  $(call fixPath, $^) -o $@"
endif
	$(Q)$(CC) $(CFLAGS) $(call fixPath, $^) -o $@ $(LFLAGS)

## COMPILATION ============================================================

# This pattern is used for compiling object files. There
# is no need to put any dependencies in the main Makefile.
# Instead, make will look at source1.o and source2.o in 
# the final executable above, and do two things:
#
#   1) Compile a .cpp file called source1.cpp into 
#      source1.o (and the same for source2.cpp, etc.)
#
#   2) Compute dependencies of source1.cpp and store
#      them in source1.d. These are then included in
#      subsequent calls to the makefile, which means
#      that objects will be automatically recompiled
#      if their dependencies change.
#
# Note: The makefiles themselves are included as dependencies
# here. This is so that all object files will be rebuilt if
# anything in any of the makefiles change.
#
# There are two rules below, one matches c files and the
# other matches cpp files.
#
MAKEFILES = Makefile patterns.mk 
%.p1: %.c $(MAKEFILES) | $(OBJDIR) $(DEPS) PRINT_INFO
ifneq ($(filter @, $(Q)),)
	@echo "CC  $< -o $(OBJDIR)/$@"
endif
	$(Q)$(CC) $(CFLAGS) $(IFLAGS) $(DFLAGS) -c $< -o $@

# This target cleans the directory. It removes all the
# object files, and also all the dependency information.
#
.PHONY: clean
clean:
ifneq ($(filter @, $(Q)),)
	@echo "CLEAN  $(OBJDIR)/*.o  $(DEPS)/*.d $(EXE)"
endif
	$(Q)-rm -rf $(OBJDIR)/*.o  $(DEPS)/*.d $(EXE)
