# the name of the target operating system
set(CMAKE_SYSTEM_NAME Microchip)
set(CMAKE_PROCESSOR_NAME 16F18346)

set(CMAKE_C_FLAGS_INIT "-mcpu=${CMAKE_PROCESSOR_NAME}")

# which compilers to use for C
set(CMAKE_C_COMPILER /opt/microchip/xc8/v2.36/bin/xc8-cc)

# adjust the default behavior of the FIND_XXX() commands:
# search programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# Never use any includes or libraries 
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE NEVER)
